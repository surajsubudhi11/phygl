#include <vector>
#include <time.h>

#include "src\Graphics\Simple2DRenderer.h"
#include "src\Graphics\window.h"
#include "src\Graphics\Sprite.h"
#include "src\Graphics\BatchRenderer2D.h"


int main() {

	using namespace phyGL;
	using namespace graphics;
	using namespace math;

	Window window("Hello Window!!", 540, 960);
	//glClearColor(1.0f, 0.0f, 1.0f, 1.0f);

	mat4 ortho = mat4::orthographic(0.0f, 16.0f, 0.0f, 9.0f, -1.0f, 1.0f);
	Shader shader("src/Shaders/basic.vs", "src/Shaders/basic.fs");
	
	shader.enable();
	shader.setUniformMat4("pr_matrix", ortho);
	//shader.setUniformMat4("ml_matrix", mat4::translation(vec3(2, 2, 0)));

	std::vector<Renderable2D*> sprites;
	srand(time(NULL));
	for (float y = 0; y < 9.0f; y += 0.15)
	{
		for (float x = 0; x < 16.0f; x += 0.15)
		{
			//sprites.push_back(new StaticSprite(x, y, 0.04f, 0.04f, math::vec4(rand() % 1000 / 1000.0f, 0, 1, 1), shader));
			sprites.push_back(new Sprite(x, y, 0.04f, 0.04f, math::vec4(rand() % 1000 / 1000.0f, 0, 1, 1)));
		}
	}

	Sprite sprite(5, 5, 4, 4, math::vec4(1, 0, 1, 1));
	Sprite sprite2(7, 1, 2, 3, math::vec4(0.2f, 0, 1, 1));
	BatchRenderer2D renderer;

	shader.setUniform2f("light_pos", vec2(4.0f, 1.5f));
	shader.setUniform4f("vert_color", vec4(0.2f, 0.3f, 0.8f, 1.0f));

	while (!window.Closed())
	{
		window.Clear();
		double x, y;
		window.getMousePosition(x, y);
		shader.setUniform2f("light_pos", vec2((float)(x * 16.0f / 960.0f), (float)((y - 9.0f) * 9.0f / 540.0f)));

		renderer.begin();

		for (int i = 0; i < sprites.size(); i++) 
		{
			renderer.submit(sprites[i]);
		}
		renderer.end();

		renderer.flush();

		std::cout << "No. of Sprites : " << sprites.size() << std::endl;
		window.Update();
	}

	//system("PAUSE");
	return 0;
}