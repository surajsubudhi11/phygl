#include "vec2.h"

namespace phyGL {
	namespace math {


		vec2::vec2()
		{
			this->x = 0.0f;
			this->y = 0.0f;
		}

		vec2::vec2(const float& x, const float& y)
		{
			this->x = x;
			this->y = y;
		}

		vec2& vec2::add(const vec2& other)
		{
			this->x += other.x;
			this->y += other.y;

			return *this;
		}

		vec2& vec2::subtract(const vec2& other)
		{
			this->x -= other.x;
			this->y -= other.y;

			return *this;
		}

		vec2& vec2::multFloat(const float& _num)
		{
			this->x *= _num;
			this->y *= _num;

			return *this;
		}

		vec2& vec2::divFloat(const float& _num)
		{
			assert(_num != 0.0f);
			
			this->x /= _num;
			this->y /= _num;

			return *this;
		}

		

		vec2 operator+(vec2 left, const vec2& right)
		{
			left.add(right);
			return left;
		}

		vec2 operator-(vec2 left, const vec2& right)
		{
			left.subtract(right);
			return left;
		}

		vec2 operator*(const float& left, vec2 right)
		{
			right.multFloat(left);
			return right;
		}

		vec2 operator/(const float& left, vec2 right)
		{
			right.divFloat(left);
			return right;
		}

		std::ostream& operator<<(std::ostream& stream, const vec2& _vec)
		{
			stream << "vec2 : ( " << _vec.x << ", " << _vec.y << ")";
			return stream;
		}

		vec2& vec2::operator=(vec2& _other)
		{
			this->x = _other.x;
			this->y = _other.y;

			return *this;
		}

		bool vec2::operator==(const vec2& _other)
		{
			return this->x == _other.x && this->y == _other.y;
		}

		bool vec2::operator!=(const vec2& _other)
		{
			return !(*this == _other);
		}

		vec2::~vec2()
		{
		}
	}
}