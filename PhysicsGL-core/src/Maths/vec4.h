#pragma once

#include <iostream>
#include <assert.h>

namespace phyGL 
{
	namespace math 
	{
		class vec4
		{
		public:
			vec4();
			vec4(const float& x, const float& y, const float& z, const float& w);

			vec4& add(const vec4& other);
			vec4& subtract(const vec4& other);

			vec4& multFloat(const float& _num);
			vec4& divFloat(const float& _num);

			vec4& operator=(vec4& _other);
			bool operator==(const vec4& _other);
			bool operator!=(const vec4& _other);

			friend vec4 operator+( vec4 left, const vec4& right);
			friend vec4 operator-( vec4 left, const vec4& right);
			friend vec4 operator*( const float& left, vec4 right);
			friend vec4 operator/( const float& left, vec4 right);

			friend std::ostream& operator<<(std::ostream& stream, const vec4& _vec);

			~vec4();

			float x, y, z, w;
		};

		
	}
}

