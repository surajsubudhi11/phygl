#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

namespace phyGL 
{
	namespace math
	{
		
		inline float toRadians(float deg)
		{
			return deg * (M_PI / 180.0f);
		}

	}
}