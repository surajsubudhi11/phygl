#pragma once

#include <iostream>
#include <assert.h>

namespace phyGL 
{
	namespace math 
	{
		class vec2
		{
		public:
			vec2();
			vec2(const float& x, const float& y);

			vec2& add(const vec2& other);
			vec2& subtract(const vec2& other);

			vec2& multFloat(const float& _num);
			vec2& divFloat(const float& _num);

			vec2& operator=(vec2& _other);
			bool operator==(const vec2& _other);
			bool operator!=(const vec2& _other);

			friend vec2 operator+( vec2 left, const vec2& right);
			friend vec2 operator-( vec2 left, const vec2& right);
			friend vec2 operator*( const float& left, vec2 right);
			friend vec2 operator/( const float& left, vec2 right);

			friend std::ostream& operator<<(std::ostream& stream, const vec2& _vec);

			~vec2();
			
			float x, y;
		};

		
	}
}

