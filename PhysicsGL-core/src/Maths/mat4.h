#pragma once


#include "math_utils.h"
#include "vec3.h"
#include "vec4.h"

namespace phyGL 
{
	namespace math
	{

		class mat4
		{
		public:
			mat4();
			mat4(float diagonal);

			static mat4 identity();

			mat4& multiply(const mat4& _other);
			friend mat4 operator*(mat4 left, const mat4& right);

			static mat4 orthographic(float _left, float _right, float _top, float _bottom, float _zmin, float _zmax);
			static mat4 perspective(float fov, float aspect_ratio, float near, float far);

			static mat4 translation(const vec3& translation);
			static mat4 rotation(float angle, const vec3& axis);
			static mat4 scale(const vec3& _scale);



			~mat4();
			union 
			{
				float elements[4 * 4];
				vec4 columns[4];
			};
		};


	}
}
