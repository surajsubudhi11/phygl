#include "vec4.h"

namespace phyGL {
	namespace math {


		vec4::vec4()
		{
			this->x = 0.0f;
			this->y = 0.0f;
		}

		vec4::vec4(const float& x, const float& y, const float& z, const float& w)
		{
			this->x = x;
			this->y = y;
			this->z = z;
			this->w = w;
		}

		vec4& vec4::add(const vec4& other)
		{
			this->x += other.x;
			this->y += other.y;
			this->z += other.z;
			this->w += other.w;

			return *this;
		}

		vec4& vec4::subtract(const vec4& other)
		{
			this->x -= other.x;
			this->y -= other.y;
			this->z -= other.z;
			this->w -= other.w;

			return *this;
		}

		vec4& vec4::multFloat(const float& _num)
		{
			this->x *= _num;
			this->y *= _num;
			this->z *= _num;
			this->w *= _num;

			return *this;
		}

		vec4& vec4::divFloat(const float& _num)
		{
			assert(_num != 0.0f);
			
			this->x /= _num;
			this->y /= _num;
			this->z /= _num;
			this->w /= _num;

			return *this;
		}

		vec4& vec4::operator=(vec4& _other) 
		{
			this->x = _other.x;
			this->y = _other.y;
			this->z = _other.z;
			this->w = _other.w;

			return *this;
		}

		bool vec4::operator==(const vec4& _other) 
		{
			return this->x == _other.x && this->y == _other.y && this->z == _other.z && this->w == _other.w;
		}

		bool vec4::operator!=(const vec4& _other)
		{
			return !(*this == _other);
		}

		vec4 operator+(vec4 left, const vec4& right)
		{
			left.add(right);
			return left;
		}

		vec4 operator-(vec4 left, const vec4& right)
		{
			left.subtract(right);
			return left;
		}

		vec4 operator*(const float& left, vec4 right)
		{
			right.multFloat(left);
			return right;
		}

		vec4 operator/(const float& left, vec4 right)
		{
			right.divFloat(left);
			return right;
		}

		std::ostream& operator<<(std::ostream& stream, const vec4& _vec)
		{
			stream << "vec4 : ( " << _vec.x << ", " << _vec.y << ", " << _vec.z << ", " << _vec.w << ")";
			return stream;
		}


		vec4::~vec4()
		{
		}
	}
}