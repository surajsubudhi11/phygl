#pragma once

#include <iostream>
#include <assert.h>

namespace phyGL 
{
	namespace math 
	{
		class vec3
		{
		public:
			vec3();
			vec3(const float& x, const float& y, const float& z);

			vec3& add(const vec3& other);
			vec3& subtract(const vec3& other);

			vec3& multFloat(const float& _num);
			vec3& divFloat(const float& _num);

			vec3& operator=(vec3& _other);
			bool operator==(const vec3& _other);
			bool operator!=(const vec3& _other);

			friend vec3 operator+( vec3 left, const vec3& right);
			friend vec3 operator-( vec3 left, const vec3& right);
			friend vec3 operator*( const float& left, vec3 right);
			friend vec3 operator/( const float& left, vec3 right);

			friend std::ostream& operator<<(std::ostream& stream, const vec3& _vec);

			~vec3();
			
			float x, y, z;
		};

		
	}
}

