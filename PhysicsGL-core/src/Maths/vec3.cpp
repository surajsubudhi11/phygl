#include "vec3.h"

namespace phyGL {
	namespace math {


		vec3::vec3()
		{
			this->x = 0.0f;
			this->y = 0.0f;
			this->z = 0.0f;
		}

		vec3::vec3(const float& x, const float& y, const float& z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}

		vec3& vec3::add(const vec3& other)
		{
			this->x += other.x;
			this->y += other.y;
			this->z += other.z;

			return *this;
		}

		vec3& vec3::subtract(const vec3& other)
		{
			this->x -= other.x;
			this->y -= other.y;
			this->z -= other.z;

			return *this;
		}

		vec3& vec3::multFloat(const float& _num)
		{
			this->x *= _num;
			this->y *= _num;
			this->z *= _num;

			return *this;
		}

		vec3& vec3::divFloat(const float& _num)
		{
			assert(_num != 0.0f);
			
			this->x /= _num;
			this->y /= _num;
			this->z /= _num;

			return *this;
		}

		vec3& vec3::operator=(vec3& _other) 
		{
			this->x = _other.x;
			this->y = _other.y;
			this->z = _other.z;

			return *this;
		}

		bool vec3::operator==(const vec3& _other) 
		{
			return this->x == _other.x && this->y == _other.y && this->z == _other.z;
		}

		bool vec3::operator!=(const vec3& _other)
		{
			return !(*this == _other);
		}

		vec3 operator+(vec3 left, const vec3& right)
		{
			left.add(right);
			return left;
		}

		vec3 operator-(vec3 left, const vec3& right)
		{
			left.subtract(right);
			return left;
		}

		vec3 operator*(const float& left, vec3 right)
		{
			right.multFloat(left);
			return right;
		}

		vec3 operator/(const float& left, vec3 right)
		{
			right.divFloat(left);
			return right;
		}

		std::ostream& operator<<(std::ostream& stream, const vec3& _vec)
		{
			stream << "vec3 : ( " << _vec.x << ", " << _vec.y << ", " << _vec.z << ")";
			return stream;
		}


		vec3::~vec3()
		{
		}
	}
}