#pragma once

#include <iostream>
#include <vector>
#include <GL\glew.h>
#include "../Maths/maths.h"
#include "../utils/file_utils.h"


namespace phyGL
{
	namespace graphics
	{
		class Shader
		{
		public:
			Shader(const char* vertPath, const char* fragPath);
			~Shader();

			void enable() const;
			void disable() const;

			void setUniform1f(const GLchar* name, float value);
			void setUniform1i(const GLchar* name, int value);
			void setUniform2f(const GLchar* name, const math::vec2& vector);
			void setUniform3f(const GLchar* name, const math::vec3& vector);
			void setUniform4f(const GLchar* name, const math::vec4& vector);
			void setUniformMat4(const GLchar* name, const math::mat4& matrix);

		private:
			GLuint m_shaderID;
			const char* m_vertPath;
			const char* m_fragPath;

			GLuint load();
			GLint getUniformLocation(const GLchar* name);
		};

		

	}
}
