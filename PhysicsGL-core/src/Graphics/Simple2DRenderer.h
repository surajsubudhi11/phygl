#pragma once

#include "Renderer2D.h"
#include <deque>
#include "StaticSprite.h"

namespace phyGL
{
	namespace graphics
	{
		class Simple2DRenderer : public Renderer2D
		{
		private:
			std::deque<const StaticSprite*> m_RenderQueue;
		public:
			void submit(const Renderable2D* renderable) override;
			void flush() override;

		};
	}
}