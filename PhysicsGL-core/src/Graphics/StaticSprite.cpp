#include "StaticSprite.h"

namespace phyGL
{
	namespace graphics
	{
		StaticSprite::StaticSprite(float x, float y, float height, float width, math::vec4 a_Color, Shader& a_Shader)
			: Renderable2D(math::vec3(x, y, 0), math::vec2(width, height), a_Color) ,m_Shader(a_Shader)
		{
			m_VertexArray = new VertexArray();

			GLfloat vertices[] =
			{
				0,		  0, 0,
				0, height, 0,
				width, height, 0,
				width,		  0, 0
			};

			GLfloat colors[] =
			{
				a_Color.x, a_Color.y, a_Color.z, a_Color.w,
				a_Color.x, a_Color.y, a_Color.z, a_Color.w,
				a_Color.x, a_Color.y, a_Color.z, a_Color.w,
				a_Color.x, a_Color.y, a_Color.z, a_Color.w
			};

			m_VertexArray->AddBuffer(new Buffers(vertices, 4 * 3, 3), 0);
			m_VertexArray->AddBuffer(new Buffers(colors, 4 * 4, 4), 1);

			GLushort indices[] = { 0, 1, 2, 2, 3, 0 };
			m_IndexBuffer = new IndexBuffers(indices, 6);

		}

		StaticSprite::~StaticSprite()
		{
			delete m_VertexArray;
			delete m_IndexBuffer;
		}
	}
}