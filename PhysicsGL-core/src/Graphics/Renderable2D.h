#pragma once

//#include "Buffers\buffers.h"
#include "Buffers\VertexArray.h"
#include "Buffers\IndexBuffer.h"

//#include "../Maths/maths.h"
#include "shader.h"

namespace phyGL
{
	namespace graphics
	{
		struct VertexData
		{
			math::vec3 vertex;
			math::vec4 color;
		};

		class Renderable2D
		{
		public:
			Renderable2D(math::vec3 a_Position, math::vec2 a_Size, math::vec4 a_Color);
			~Renderable2D();


			inline const math::vec3& getPosition() const { return m_Position; }
			inline const math::vec2& getSize() const { return m_Size; }
			inline const math::vec4& getColor() const { return m_Color; }

		protected:
			math::vec3 m_Position;
			math::vec4 m_Color;
			math::vec2 m_Size;

		};

		
	}
}
