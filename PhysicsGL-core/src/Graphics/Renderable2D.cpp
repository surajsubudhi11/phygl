#include "Renderable2D.h"

namespace phyGL
{
	namespace graphics
	{
		Renderable2D::Renderable2D(math::vec3 a_Position, math::vec2 a_Size, math::vec4 a_Color)
			: m_Position(a_Position), m_Size(a_Size), m_Color(a_Color)
		{


		}

		Renderable2D::~Renderable2D()
		{

		}
	}
}