#pragma once
#include "Renderable2D.h"

namespace phyGL
{
	namespace graphics
	{
		class StaticSprite : public Renderable2D
		{
		public:
			StaticSprite(float x, float y, float height, float width, math::vec4 a_Color, Shader& a_Shader);
			~StaticSprite();

			inline const VertexArray* getVAO() const { return m_VertexArray; }
			inline const IndexBuffers* getIBO() const { return m_IndexBuffer; }
			inline Shader& getShader() const { return m_Shader; }

		protected:
			VertexArray* m_VertexArray;
			IndexBuffers* m_IndexBuffer;
			Shader& m_Shader;
		};


	}
}
