#pragma once

#include "Renderer2D.h"
#include "Buffers\IndexBuffer.h"

namespace phyGL
{
	namespace graphics
	{

#define RENDERER_MAX_SPRITS 60000
#define RENDERER_VERTEX_SIZE sizeof(VertexData)
#define RENDERER_SPRITE_SIZE RENDERER_VERTEX_SIZE * 4
#define RENDERER_BUFFER_SIZE RENDERER_SPRITE_SIZE * RENDERER_MAX_SPRITS
#define RENDERER_INDICES_SIZE RENDERER_MAX_SPRITS * 6

#define SHADER_VERTEX_INDEX 0
#define SHADER_COLOR_INDEX 1


		class BatchRenderer2D : public Renderer2D
		{
		public:
			BatchRenderer2D();
			~BatchRenderer2D();

			void begin();
			void submit(const Renderable2D* renderable) override;
			void end();
			void flush() override;

		private:
			GLuint m_VAO;
			GLuint m_VBO;
			IndexBuffers* m_IBO;
			GLsizei m_IndexCount;
			VertexData* m_VertexBuffer;

			void init();
		};

	}
}