#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <iostream>


namespace phyGL {
	namespace graphics {
	

#define MAX_KEYS	1024
#define MAX_BUTTONS 32

		class Window
		{
		public:
			Window(const char* name, int height, int width);
			~Window();
			
			void Clear() const;
			void Update() const;
			bool Closed() const;

			bool isKeyPressed(unsigned int keycode) const;
			bool isButtonPressed(unsigned int keycode) const;
			void getMousePosition(double& x, double& y) const;
		private:
			const char *m_name;
			int m_height;
			int m_width;
			bool m_isClosed;
			GLFWwindow* m_window;

			bool m_keys[MAX_KEYS];
			bool m_buttons[MAX_BUTTONS];
			double mx, my;

			bool init();
			friend static void key_callback(GLFWwindow* _window, int key, int scanecode, int action, int mods);
			friend static void mouse_callback(GLFWwindow* _window, int button, int action, int mods);
			friend static void curssor_pos_callback(GLFWwindow* _window, double _xpos, double _ypos);
		};
	}
}
