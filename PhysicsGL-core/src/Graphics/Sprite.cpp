#include "Sprite.h"

namespace phyGL
{
	namespace graphics
	{
		Sprite::Sprite(float x, float y, float height, float width, math::vec4 a_Color)
			: Renderable2D(math::vec3(x, y, 0), math::vec2(width, height), a_Color)
		{


		}

		Sprite::~Sprite()
		{

		}
	}
}