#pragma once
#include "Renderable2D.h"

namespace phyGL
{
	namespace graphics
	{
		class Sprite : public Renderable2D
		{
		public:
			Sprite(float x, float y, float height, float width, math::vec4 a_Color);
			~Sprite();

		};


	}
}
