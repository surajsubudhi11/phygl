#pragma once

#include <vector>
#include <GL\glew.h>

#include "buffers.h"

namespace phyGL 
{
	namespace graphics
	{
		class VertexArray
		{
		public:
			VertexArray();
			~VertexArray();

			void AddBuffer(Buffers* a_Buffer, GLuint index);
			void Bind() const;
			void Unbind() const;

		private:
			GLuint m_ArrayID;
			std::vector<Buffers*> m_Buffers;
		};
	}
}
