#pragma once

#include <GL\glew.h>

namespace phyGL
{
	namespace graphics
	{
		class Buffers
		{
		public:
			Buffers(GLfloat* data, GLsizei count, GLuint componentCount);
			~Buffers();

			void Bind() const;
			void UnBind() const;

			inline GLuint getComponentCount() const { return m_ComponentCount; }

		private:
			GLuint m_BufferID;
			GLuint m_ComponentCount;
		};
				

	}
}