#pragma once

#include <GL\glew.h>

namespace phyGL
{
	namespace graphics
	{
		class IndexBuffers
		{
		public:
			IndexBuffers(GLushort* data, GLsizei count);
			IndexBuffers(GLuint* data, GLsizei count);
			~IndexBuffers();

			void Bind() const;
			void UnBind() const;

			inline GLuint getCount() const { return m_Count; }

		private:
			GLuint m_BufferID;
			GLuint m_Count;
		};
	}
}
