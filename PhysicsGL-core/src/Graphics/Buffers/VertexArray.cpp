#include "VertexArray.h"


namespace phyGL
{
	namespace graphics
	{
		VertexArray::VertexArray()
		{
			glGenVertexArrays(1, &m_ArrayID);
		}

		void VertexArray::AddBuffer(Buffers* a_Buffer, GLuint index)
		{
			Bind();
			a_Buffer->Bind();

			glEnableVertexAttribArray(index);
			glVertexAttribPointer(index, a_Buffer->getComponentCount(), GL_FLOAT, GL_FALSE, 0, 0);

			a_Buffer->UnBind();
			Unbind();
		}

		void VertexArray::Bind() const
		{
			glBindVertexArray(m_ArrayID);
		}

		void VertexArray::Unbind() const
		{
			glBindVertexArray(0);
		}

		VertexArray::~VertexArray()
		{
			for (int i = 0; i < m_Buffers.size(); i++)
				delete m_Buffers[i];

			glDeleteVertexArrays(1, &m_ArrayID);
		}
	}
}