#include "window.h"


namespace phyGL 
{
	namespace graphics
	{


		// Function Prototypes
		//void key_callback(GLFWwindow* _window, int key, int scanecode, int action, int mods); 

		Window::Window(const char* name, int height, int width)
		{
			m_name = name;
			m_height = height;
			m_width = width;
			if (!init())
				glfwTerminate();

			for (int i = 0; i < MAX_BUTTONS; i++) {
				m_buttons[i] = false;
			}

			for (int i = 0; i < MAX_KEYS; i++) {
				m_keys[i] = false;
			}
		}

		bool Window::init() 
		{
			if (!glfwInit()) 
			{
				std::cerr << "Failed to initialize GLFW!!" << std::endl;
				return false;
			}
			m_window = glfwCreateWindow(m_width, m_height, m_name, NULL, NULL);
			if (!m_window) 
			{
				std::cerr << "Failed to create GLFW Window!!!" << std::endl;
				return false;
			}

			glfwMakeContextCurrent(m_window);
			glfwSetWindowUserPointer(m_window, this);
			glfwSetKeyCallback(m_window, key_callback);
			glfwSetMouseButtonCallback(m_window, mouse_callback);
			glfwSetCursorPosCallback(m_window, curssor_pos_callback);
			
			if (glewInit() != GLEW_OK) 
			{
				std::cerr << "Failed to initialize GLEW!!" << std::endl;
				return false;
			}
			
			return true;
		}

		bool Window::isKeyPressed(unsigned int keycode) const
		{
			if (keycode >= MAX_KEYS)
				return false;

			return m_keys[keycode];
		}

		bool Window::isButtonPressed(unsigned int buttoncode) const
		{
			if (buttoncode >= MAX_BUTTONS)
				return false;

			return m_buttons[buttoncode];
		}

		void Window::getMousePosition(double& x, double& y) const
		{
			x = mx;
			y = my;
		}

		void Window::Clear() const
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		}

		void Window::Update() const
		{
			glfwPollEvents();
			glfwSwapBuffers(m_window);
		}

		bool Window::Closed() const
		{
			return glfwWindowShouldClose(m_window) == 1;
		}


		Window::~Window()
		{
			glfwTerminate();
		}

		void key_callback(GLFWwindow* _window, int key, int scanecode, int action, int mods) 
		{
			Window * win = (Window*)glfwGetWindowUserPointer(_window);

			win->m_keys[key] = action != GLFW_RELEASE;
		}

		void mouse_callback(GLFWwindow* _window, int button, int action, int mods)
		{
			Window * win = (Window*)glfwGetWindowUserPointer(_window);

			win->m_buttons[button] = action != GLFW_RELEASE;
		}

		void curssor_pos_callback(GLFWwindow* _window, double _xpos, double _ypos)
		{
			Window * win = (Window*)glfwGetWindowUserPointer(_window);

			win->mx = _xpos;
			win->my = _ypos;
		}

	}
}